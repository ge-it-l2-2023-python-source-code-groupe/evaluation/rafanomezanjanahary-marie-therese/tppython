def Operation_Conversion_de_type():
    print(f"    exo2.11.3:Opérations et Conversions des types")
    print(f"Premiere instruction:{str(4) * int('3')}")
    print(f"Deuxieme instruction:{int('3') + float('3.2')}")
    print(f"Troisieme instruction:(str(3) * float('3.2')):non valide.")
    print(f"Quatrieme instruction:{str(3/4) * 2}")