def Friedman():
    print(f"    exo2.11.1:Nombre de Friedman.")
    print(f"Test d'expression mathematique:\nL'expression 7+3\u2076 = {7+3**6}--est un nombre de Friedman")
    print(f"L'expression (3+4)\u2073 = {(3+4)**3}--est un nombre de Friedman")
    print(f"L'expression 3\u2076-5 = {3**6-5}--n'est pas un nombre de Friedman")
    print(f"L'expression (1+2\u2078)*5 = {(1+2**8)*5}--est un nombre de Friedman")
    print(f"L'expression (2+1\u2078)\u2077 = {(2+1**8)**7}--est un nombre de Friedman")


