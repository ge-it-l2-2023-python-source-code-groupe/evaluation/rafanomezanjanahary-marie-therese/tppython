from chapitres.chap6_Testes.exo6_7_1 import *
from chapitres.chap6_Testes.exo6_7_2 import *
from chapitres.chap6_Testes.exo6_7_3 import *
from chapitres.chap6_Testes.exo6_7_4 import *
from chapitres.chap6_Testes.exo6_7_5 import *
from chapitres.chap6_Testes.exo6_7_6 import *
from chapitres.chap6_Testes.exo6_7_7 import *
from chapitres.chap6_Testes.exo6_7_8 import *
from chapitres.chap6_Testes.exo6_7_9 import *
from chapitres.chap6_Testes.exo6_7_10 import *
from tools.console import*


def menu():
    print(f""" 
        Chapitre 6:Tests
    1-Jours de la semaine
    2-Sequence Complémentaire d'un brin d'ADN
    3-Minimum d'une liste
    4-Fréquences des acides anminées
    5-Notes et mentions d'un étudiants
    6-Nombre pairs et impairs entre 0 à 20
    7-Conjoncture de Syracuse
    8-Attribution de la structure secondaire des acides aminés d’une protéine
    9-Determination des nombres premiers entre 0 et 100
    10-Recherche d’un nombre par dichotomie 
    11-Revenir au menu principale""")

def Menu_Testes():
    
    while True:
        menu()
        choix=input("Entrer votre choix dans cette chapitre:")

        if choix=="":
            print("======>Entrer quelque chose")
            continue
        if choix == "1":
            clear()
            TestSemaine()
            cliquez()
        elif choix == "2":
            clear()
            SequenceAA()
            cliquez()
        elif choix == "3":
            clear()
            ValMin()
            cliquez()
        elif choix == "4":
            clear()
            FrequenceAA()
            cliquez()
        elif choix == "5":
            clear()
            Notes()
            cliquez()
        elif choix == "6":
            clear()
            Pair_Impair()
            cliquez()
        elif choix == "7":
            clear()
            Conjoncture_de_Syracuse()
            cliquez()
        if choix == "8":
            clear()
            Phi_Psi()
            cliquez()
        elif choix == "9":
            clear()
            NombrePremier()
            cliquez()
        elif choix == "10":
            clear()
            DevinetteDichotomie()
            cliquez()
        elif choix == "11":            
            print("---- Fin du programme")
            break
            
        else:
            print("xxxxxxxx Choix invalide.Recommencer!! xxxxxxxx")
            continue