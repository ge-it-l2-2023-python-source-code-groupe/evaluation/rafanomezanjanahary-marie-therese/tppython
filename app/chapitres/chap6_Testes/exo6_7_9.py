def NombrePremier():
    print(f"    exo6.7.8:Determination des nombres premiers entre 0 et 100:")

    print(f"1ère méthodes:") 
    
    print(f"Les nombres premiers entre 0 et 100 sont:")
    count = 0
    n =2
    limit = 100
    if n > 1:
        while n <= limit:
            if n == 2:
                count += 1
                print(n,end=" ")
            else:
                i,div = 1,0
                while i <= n:
                    if n % i == 0:
                        div += 1
                    i += 1
                if div <= 2:
                    count += 1
                    print(n,end=" ")
            n += 1
    print(f"\nLe nombre des nombre premier entre 0 et 100 est:{count}")
    
    print(f"\n2ème méthodes:")
    nbre = [2]
    for i in range(4,101,1):
        counter = 0
        for j in range(0,len(nbre),1):
            if i%nbre[j] != 0 :
                counter += 1
        if counter == len(nbre):
            nbre.append(i)
    print(nbre,"\n")
    print(f" Il y a {len(nbre)} nombre entre 0 et 100.")
        