def Phi_Psi():
    print(f"    exo6.7.8:Attribution de la structure secondaire des acides aminés d’une protéine")
    liste=[
       [-58.8, 43.1],[-73.9, -40.6],[-53.7, -35.7],
       [-80.6, -26.0],[-68.5, 135.0],[-64.9, -23.5],
       [-66.9, -45.5],[-69.6, -41.0],[-62.7, -37.5],
       [-68.2, -38.3],[-61.2, -49.1],[-59.7, -41.1]
       ]

    phiNormal = -57.0
    psiNormal = -47.0
    tolerance = 30.0
    for i ,(phi,psi) in enumerate(liste,start=0):
        if (phi - phiNormal) <= tolerance and (psi - psiNormal) <= tolerance:
            print(f"[{phi},{psi}] est en hélice.")
        else:
            print(f"{i} [{phi},{psi}] n'est pas en hélice.") 

