def Conjoncture_de_Syracuse():
    print(f"    exo6.7.7:Conjoncture de Syracuse")
    n=int(input("Entrez un nombre entier positif:"))
    syracuse =[n]
    trivial=[]
    while len(syracuse) < 50:
        n = n // 2 if n%2 == 0 else n*3+1
        syracuse += [n]
        if n == 1:
            trivial = syracuse[-3:]     
    print(syracuse)
    print(f"oui ! La conjencture de Syracuse est vérifié.\n Les nombre qui constituent le cycle trivial sont :{trivial}")