def Pair_Impair():
    print(f"    exo6.7.6:Nombre pairs et impairs entre 0 à 20:")
    for i in range(21):
        print(i)
    print('~'*24)
    for i in range(11):
        if i%2 == 0:
            print(i)
    print('~'*24)
    for i  in range(10,20):
        if i%2 != 0:
            print(i)

    #autre méthode:
    #for i in range(21):
        #if(i%2 ==0 and i <= 10) or (i%2 != 0 and i > 10):
         #   print(i,end="")

