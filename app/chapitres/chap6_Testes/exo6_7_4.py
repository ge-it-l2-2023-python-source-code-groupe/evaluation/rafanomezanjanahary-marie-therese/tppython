def FrequenceAA():
    print(f"    exo6.7.4:Fréquences des acides anminées:")
    aa=['A','R','A','W','W','A','W','A','R','W','W','R','A','G']
    print(aa)
    alanine,arginine,trytophane,glycine = 0,0,0,0
    for i in aa:
        if i == 'A':
            alanine += 1
        elif i == 'R':
            arginine += 1
        elif i == 'W':
            trytophane += 1
        elif i == 'G':
            glycine += 1
    print(f"Les fréquences:\n   alanine (A):{alanine}\n   arginine (R):{arginine}\n   trypophane (W):{trytophane}\n   glycine (G):{glycine}")
   


