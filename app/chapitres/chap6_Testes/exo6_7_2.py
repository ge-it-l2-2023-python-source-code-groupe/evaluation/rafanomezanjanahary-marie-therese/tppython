def SequenceAA():
    print(f"    exo6.7.2:Sequence Complémentaire d'un brin d'ADN:")
    sequence=['A','C','G','T','T','A','G','C','T','A','A','C','G']
    print(sequence)
    print('-'*24)
    for i in range(len(sequence)):
        if sequence[i]=='A':
            print('T',end='_')
        elif sequence[i]=='T':
            print('A',end='_')
        elif sequence[i]=='C':
            print('G',end='_')
        else:
            print('C',end='_')

    # autre méthode:
    #for i,value in enumerate(sequence):
    #    if value == "A":
    #       sequence[i]="T"
    #    elif value == "T":
    #      sequence = "A"
    #   elif value == "C":
     #    sequence = "G"
     #  elif value == "G":
      #   sequence = "C"
    #print(sequence)





 
        
