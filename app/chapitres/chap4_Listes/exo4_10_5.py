def List_Indice():
#1):
    print(f"    exo4.10.5:Liste et Indices de la semaine:")
    semaine=['lundi','mardi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']
    print(semaine)
    print('-'*24)
#2):    
    print(semaine[4])
    print('-'*24)
#3):
    temp=semaine[0]
    semaine[0]=semaine[6]
    semaine[6]=temp
    print(semaine)
    print('-'*24)
#4):
    print((semaine[6]*12))
    print('-'*24)