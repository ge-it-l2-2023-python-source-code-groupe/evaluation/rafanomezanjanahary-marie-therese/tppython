from chapitres.chap4_Listes.exo4_10_1 import *
from chapitres.chap4_Listes.exo4_10_2 import *
from chapitres.chap4_Listes.exo4_10_3 import *
from chapitres.chap4_Listes.exo4_10_4 import *
from chapitres.chap4_Listes.exo4_10_5 import *
from chapitres.chap4_Listes.exo4_10_6 import *
from tools.console import*

def menu():
    print(""" 
        Chapitre4:Listes
    1-Jours de la semaine
    2-Saisons
    3-Table du multiplication par 9
    4-Nombre pairs
    5-Liste et Indices de la semaine
    6-Liste et Ranges des fonctions
    7-retour au menu principale""")

def Menu_Listes():
    
    while True:
        menu()
        choix=input("Entrer votre choix dans cette chapitre:")
        if choix=="":
                print("======>Entrer quelque chose")
                continue
        if choix == '1':
            Semaine()
            cliquez()
        elif choix == '2':
            Saison()
            cliquez()
        elif choix == '3':
            Table9()
            cliquez()
        elif choix == '4':
            NombrePair()
            cliquez()
        elif choix == '5':
            List_Indice()
            cliquez()
        elif choix == '6':
            List_Range()
            cliquez()
        elif choix == '7':
            print("retour au menu principale")
            break
        else:
            print("xxxxxxxx Choix invalide!!!! Recommencer xxxxxxxx")

