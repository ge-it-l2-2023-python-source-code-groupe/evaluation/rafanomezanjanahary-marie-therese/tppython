def Semaine():
#1):
    print(f"    exo4.10.1:Jours de la semaine:")
    semaine=['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']
    print(semaine)

    print(semaine[0:5])#ou:
    print(semaine[5:])
    
#2):
    print(semaine[-7:-2])#ou:
    print(semaine[-2:])
    
#3):
    print(semaine[-1])#ou:
    print(semaine[6])
    
#4):
    print(semaine[-1:-8:-1])