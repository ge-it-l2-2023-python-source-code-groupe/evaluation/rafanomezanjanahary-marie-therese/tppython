def Boucle_Base():
    #1er methode:
    print(f"    exo5.4.1:Boucle de Base:")
    animal = ["vache","souris","levure","bacterie"]
    for i in range(4):
        print(animal[i])

    print ('-'*24)
    #2eme methode:
    #animaux = ["vache","souris","levure","bacterie"]
    #for animal in animaux:
    #   print(animal)

    #3eme methodes:
    #print ('-'*24)
    #animaux = ["vache","souris","levure","bacterie"]
    #i = 0   
    #while i < 4:
    #   print(animaux[i])
     #  i = i+1