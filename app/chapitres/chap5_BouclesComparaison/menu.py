from chapitres.chap5_BouclesComparaison.exo5_4_1 import *
from chapitres.chap5_BouclesComparaison.exo5_4_2 import *
from chapitres.chap5_BouclesComparaison.exo5_4_3 import *
from chapitres.chap5_BouclesComparaison.exo5_4_4 import *
from chapitres.chap5_BouclesComparaison.exo5_4_5 import *
from chapitres.chap5_BouclesComparaison.exo5_4_6 import *
from chapitres.chap5_BouclesComparaison.exo5_4_7 import *
from chapitres.chap5_BouclesComparaison.exo5_4_8 import *
from chapitres.chap5_BouclesComparaison.exo5_4_9 import *
from chapitres.chap5_BouclesComparaison.exo5_4_10 import *
from chapitres.chap5_BouclesComparaison.exo5_4_11 import *
from chapitres.chap5_BouclesComparaison.exo5_4_12 import *
from chapitres.chap5_BouclesComparaison.exo5_4_13 import *
from chapitres.chap5_BouclesComparaison.exo5_4_14 import *
from tools.console import*

def menu():
    print(f""" 
        chapitre 5:Boucles et comparaisons.
    1-Boucle de Base
    2-Listes de semaine
    3-Liste de nombres entre 0 à 10
    4-Nombre pairs et Impairs
    5-Calcul de la moyenne
    6-Produit de nombre consécutifs
    7-Trianle Droite
    8-Triangle inversé
    9-Triangle à Gauche
    10-Pyramide
    11-Parcours matrice
    12-Parcours Demi-matrice
    13-Sauts de Puce
    14-Suite de Fibonacci 
    15-retour au menu principale""")

def Menu_Boucle():
    
    while True:
        menu()
        choix=input("Entrer votre choix dans cette chapitre:")
        
        
        if choix=="":
            print("======>Entrer quelque chose")
            continue
        if choix == '1':
            clear()
            Boucle_Base()
            cliquez()
        elif choix == '2':
            clear()
            ListeSemaine()
            cliquez()
        elif choix == '3':
            clear()
            Liste10()
            cliquez()
        elif choix == '4':
            clear()
            NombrePairs()
            cliquez()
        elif choix == '5':
            clear()
            Moyen()
            cliquez()
        elif choix == '6':
            clear()
            Produit()
            cliquez()
        elif choix == '7':
            clear()
            TriangleADroite()
            cliquez()
        if choix == '8':
            clear()
            TriangleInversé()
            cliquez()
        elif choix == '9':
            clear()
            TriangleGauche()
            cliquez()
        elif choix == '10':
            clear()
            Pyramide()
            cliquez()
        elif choix == '11':
            clear()
            Matrice()
            cliquez()
        elif choix == '12':
            clear()
            DemiMatrice()
            cliquez()
        elif choix == '13':
            clear()
            SautDePuce()
            cliquez()
        elif choix == '14':
            clear()
            fibonacci()
            cliquez()
        elif choix == '15':
            print("retour au menu principale")
            break
        else:
            print("xxxxxxxx Choix invalide!! Recommencer xxxxxxxx")
            continue

