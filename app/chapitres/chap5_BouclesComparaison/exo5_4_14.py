def fibonacci():
    print(f"    exo5.4.14:Suite de Fibonacci:")
    fibo = [0, 1]
    print(f"Les 15 premiers termes de la suite Fibonacci sont:\n{fibo[0]}{fibo[1]}",end=" ")
    i = len(fibo)
    while i <= 15:
        fibo += [fibo[i-1] + fibo[i-2]]
        print(fibo[i] ,end="|")
        i +=1
    print("\n\n le rapport entre l'élément n et n-1 est :")
    i =2
    while i <= 15:
        print(f"{fibo[i]}/{fibo[i-1]}={(fibo[i]/fibo[i-1]):.8f}")
        i += 1
        print(f"\n Le rapport tend vers une constante qui est le nombre d'or Phi:{fibo[i-1]/fibo[i-2]:.8f}")
        