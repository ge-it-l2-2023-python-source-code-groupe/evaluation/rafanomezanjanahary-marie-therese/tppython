def Matrice():
    print(f"    exo5.4.11:Parcours matrice:")
    dimension = input("Entrez la dimension de la matrice carrée(entier positif): ")
    while not dimension.isdigit() or int(dimension) <= 0:
        print("Saisie non valide!! \n Veuillez entrer un nombre positive")
        dimension = input("La dimension de la matrice carré est :")
    dim = int(dimension)    
    for i in range(1, dim + 1):
        for j in range(1, dim + 1):
            print(f"{i:4} {j:4}")


