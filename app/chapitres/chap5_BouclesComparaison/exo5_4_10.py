def Pyramide():
    print(f"    exo5.4.10:Pyramide:")
    reponse = input("Entrer un nombre de ligne (entier positif):")
    while not reponse.isdigit() or int(reponse) <= 0:
        print(" Saisie no valide!!\n Veuillez entrer un nombre positive")
        reponse =input("Le nombre de ligne de pyramide:")
    n = int(reponse)
    count = 1
    for i in range(0,n):
        print(f"{' '*(n-i)}{'*'*count}")
        count += 2
    