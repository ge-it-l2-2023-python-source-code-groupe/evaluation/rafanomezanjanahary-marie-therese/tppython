def Moyen():
    print(f"    exo5.4.5:Calcul de la moyenne:")
    note = [14,9,6,8,12]
    moyenne = 0
    somme = 0
    i=0
    while i < len(note):
        somme += note[i]
        i=i+1

    moyenne = somme/len(note)
    print(note)
    print (f" La moyenne est:{moyenne:.2f}/20")
