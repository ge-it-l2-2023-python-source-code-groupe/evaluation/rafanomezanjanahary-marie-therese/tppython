from chapitres.chap3_Affichages.exo3_6_1 import *
from chapitres.chap3_Affichages.exo3_6_2 import *
from chapitres.chap3_Affichages.exo3_6_3 import *
from chapitres.chap3_Affichages.exo3_6_4 import *
from chapitres.chap3_Affichages.exo3_6_5 import *
from tools.console import*

def menu():
    print(""" 
        Chapitre 3:Affichages
    1-Affichages
    2-Poly-A
    3-Poly-A et Poly-GC
    4-Ecriture formatée
    5-Ecriture formatée 2
    6-Retour au menu principale """)


def  Menu_Affichage():
   
    while True:
        menu()
       
        choix=input("Entrer votre choix dans cette chapitre:")
        if choix=="":
            print("======>Entrer quelque chose")
            continue
        if choix == '1':
            
            Affichage()
            cliquez()
        elif choix == '2':
            
            PolyA()
            cliquez()
        elif choix == '3':
            
            BasesAA()
            cliquez()
        elif choix == '4':
            
            Afficher()
            cliquez()
        elif choix == '5':
            
            Pourcentage()
            cliquez()
        elif choix == '6':
            print("retour au menu principale")
            break
        else:
            print('xxxxxxxx Choix invalide!! Recommencer xxxxxxxx ')
            continue
            
